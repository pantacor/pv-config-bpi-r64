# we saveenv so that the random mac addr chosen by uboot gets
# remembered ... (in ethaddr - patch uboot in pv/2020.10 branch) 
saveenv

setenv pv_baseargs "root=/dev/ram rootfstype=ramfs init=/init pv_storage.device=LABEL=pvroot pv_storage.fstype=ext4"
setenv pv_mmcdev 1
setenv pv_mmcboot 1
setexpr envloadaddr ${scriptaddr}
echo "ENVLOAD TO: ${envloadaddr}"
part size mmc ${pv_mmcdev} 2 pv_config_size
if test "${pv_config_size}" = "800"; then
	echo "Found PV OEM Config at mmc ${pv_mmcdev}:2"
	if test -z "${localargs}"; then
		part start mmc ${pv_mmcdev} 2 pv_config_start
		mmc read ${envloadaddr} ${pv_config_start} ${pv_config_size}
		env import ${envloadaddr} 0x1000
	fi
	setenv pv_mmcdata 3
else
	setenv pv_mmcdata 2
fi
echo Loading pantavisor uboot.txt
load mmc ${pv_mmcdev}:${pv_mmcdata} ${envloadaddr} /boot/uboot.txt; setenv uboot_txt_size ${filesize}

setenv pv_try; env import ${envloadaddr} ${uboot_txt_size}

echo "Loading pv.env"
load mmc ${pv_mmcdev}:${pv_mmcboot} ${envloadaddr} pv.env; setenv pv_env_size ${filesize}
setenv pv_trying; env import ${envloadaddr} ${pv_env_size}

if env exists pv_try; then
	if env exists pv_trying && test ${pv_trying} = ${pv_try}; then
		echo "Pantavisor boots checkpoint revision ${pv_rev} after failed try-boot of revision: ${pv_try}"
		setenv pv_trying
		env export ${envloadaddr} pv_trying; setenv pv_env_size ${filesize};
		save mmc ${pv_mmcdev}:${pv_mmcboot} ${envloadaddr} pv.env ${pv_env_size}
		setenv boot_rev ${pv_rev}
		setenv boot_cmdxfix ${pv_revcmd}
	else
		echo "Pantavisor boots try-boot revision: ${pv_try}"
		setenv pv_trying ${pv_try}
		env export ${envloadaddr} pv_trying; setenv pv_env_size ${filesize};
		save mmc ${pv_mmcdev}:${pv_mmcboot} ${envloadaddr} pv.env ${pv_env_size}
		setenv boot_rev ${pv_trying}
		setenv boot_cmdxfix ${pv_trycmd}
	fi
else
	echo "Pantavisor boots revision: ${pv_rev}"
	setenv boot_rev ${pv_rev}
	setenv boot_cmdxfix ${pv_revcmd}
fi

echo "Pantavisor loading FDT at ${fdt_addr_r}"
load mmc ${pv_mmcdev}:${pv_mmcdata} ${fdt_addr_r} /trails/${boot_rev}/bsp/${fdtfile}
fdt addr ${fdt_addr_r}; setexpr fdt_size ${filesize}

echo "Pantavisor bootargs: ${pv_baseargs} pv_try=${pv_try} pv_rev=${boot_rev} panic=2 ${boot_cmdxfix} ${configargs} ${localargs}"
setenv bootargs "${pv_baseargs} pv_try=${pv_try} pv_rev=${boot_rev} panic=2 ${boot_cmdxfix} ${configargs} ${localargs}"
echo "Pantavisor kernel load: load mmc ${pv_mmcdev}:${pv_mmcdata} ${kernel_addr_r} /trails/${boot_rev}/.pv/pv-kernel.img"
load mmc ${pv_mmcdev}:${pv_mmcdata} ${kernel_addr_r} /trails/${boot_rev}/.pv/pv-kernel.img; setenv kernel_size ${filesize}
echo "Pantavisor initrd load: load mmc ${pv_mmcdev}:${pv_mmcdata} ${ramdisk_addr_r} /trails/${boot_rev}/.pv/pv-initrd.img"
load mmc ${pv_mmcdev}:${pv_mmcdata} ${ramdisk_addr_r} /trails/${boot_rev}/.pv/pv-initrd.img; setenv rd_size ${filesize}
setexpr rd_offset ${ramdisk_addr_r} + ${rd_size}
setenv i 0
while load mmc ${pv_mmcdev}:${pv_mmcdata} ${rd_offset} /trails/${boot_rev}/.pv/pv-initrd.img.${i}; do
	echo "Pantavisor initrd addon loaded: load mmc ${mmcdev}:${mmcdata} ${rd_offset} /trails/${boot_rev}/.pv/pv-initrd.img.${i}"
	setexpr i ${i} + 1
	setexpr rd_size ${rd_size} + ${filesize}
	setexpr rd_offset ${rd_offset} + ${filesize}
done

# disable relocation of fdt and initrd in booti
setenv fdt_high 0xffffffff
setenv initrd_high 0xffffffff

echo "Pantavisor go... : booti ${kernel_addr_r} ${ramdisk_addr_r}:${rd_size} ${fdt_addr_r}"
booti ${kernel_addr_r} ${ramdisk_addr_r}:${rd_size} ${fdt_addr_r}
echo "Failed to boot step, rebooting"; sleep 5; reset;

