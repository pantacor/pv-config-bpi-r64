#!/bin/bash
set -x

preloader=mt7622_ATF_32_64_release.img

IN_DIR="$1"
OUT_FILE="$2"

if [ -d "${IN_DIR}" ]; then
	dd if=${IN_DIR}/BPI-R2-HEAD440-0k.img of=$OUT_FILE conv=notrunc
	dd if=${IN_DIR}/BPI-R2-HEAD1-512b.img of=$OUT_FILE bs=512 seek=1 conv=notrunc
	dd if=${IN_DIR}/preloader_bpi-r64_forsdcard-2k.img of=$OUT_FILE bs=1k seek=2 conv=notrunc
	dd if=${IN_DIR}/${preloader} of=$OUT_FILE bs=1k seek=512 conv=notrunc
fi

